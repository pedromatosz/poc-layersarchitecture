import "./style.styl"

const Layout = ({ children }: any) => {
	return (
		<div className="layout">
			{children}
		</div>
	)
}

export default Layout
