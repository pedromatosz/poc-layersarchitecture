import CounterView from "./view"
import { useState } from "react"
import { ICounter } from './interfaces'

const Counter: React.FC<ICounter> = () => {
	const [counter, setCounter] = useState(0)

	const increase = () => {
		setCounter(counter + 1)
	}

	const decrease = () => {
		setCounter(counter - 1)
	}

	const reset = () => {
		setCounter(0)
	}

	return (
		<CounterView counter={counter} increase={increase} decrease={decrease} reset={reset} />
	)
}

export default Counter
