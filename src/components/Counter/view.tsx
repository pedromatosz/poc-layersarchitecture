import "./style.styl"

const CounterView = ({
	counter,
	increase,
	decrease,
	reset
}: any) => {
	return(
		<div className="counter">
			{ counter }
			<div className="counter_btn-container">
				<button onClick={increase}> increment </button>
				<button onClick={decrease}> decrease </button>
				<button onClick={reset}> reset </button>
			</div>
		</div>
	)
}

export default CounterView
