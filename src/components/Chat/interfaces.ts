export interface IChat {

}

export interface IChatView {
	chat: string[]
	onTypeText: (e: any) => void
	sendMessage: () => void
	textField: string
}
