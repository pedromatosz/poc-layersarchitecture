import { IChatView } from "./interfaces";
import { ChatComponent, ChatWrapper } from "./style";

const ChatView: React.FC<IChatView> = ({
	chat,
	onTypeText,
	sendMessage,
	textField
}) => {
	return (
		<ChatComponent>
			<ChatWrapper>
				{ chat.map((item: string) => {
					return <p>{ item }</p>
				}) }
			</ChatWrapper>
			<div>
				<input type="text" onChange={onTypeText} value={textField}/>
				<button onClick={sendMessage}> Send </button>
			</div>
		</ChatComponent>
	)
}

export default ChatView
