import styled from 'styled-components'

export const ChatComponent = styled.div`
	border: 2px solid red;
	padding: 10px;
`

export const ChatWrapper = styled.div`
	font-size: 12px;
	text-align: right;
	border: 2px solid black;
	padding: 10px;
	margin: 20px;
	min-height: 50px;
`
