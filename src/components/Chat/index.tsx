import ChatView from "./view"
import { useState } from "react"
import { IChat } from './interfaces'

const Chat: React.FC<IChat> = () => {
	const [textField, setTextField] = useState('')
	const [chat, setChat] = useState<string[]>([])

	const sendMessage= () => {
		textField ? setChat([...chat, textField]) : null
		setTextField('')
	}

	const onTypeText = (e: any) => {
		setTextField(e.target.value)
	}

	return (
		<ChatView chat={chat} onTypeText={onTypeText} sendMessage={sendMessage} textField={textField} />
	)
}

export default Chat
