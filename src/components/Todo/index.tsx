import {
	TodoComponent,
	FormWrapper,
	TodoCard,
	NameField,
	DescriptionField,
	ComplexityField,
	SubmitButton,
	TodoCardsWrapper
} from './style'
import { ITodo, ITodoCard } from './interfaces'
import { useState } from "react"

const Todo: React.FC<ITodo> = ({

}) => {
	const [ todoList, setTodoList ] = useState<ITodoCard[]>([])
	const [ currentTodo, setCurrentTodo ] = useState<ITodoCard>({
		name: '',
		description: '',
		complexity: 0
	})

	const complexity = [
		1,
		2,
		3,
		4,
		5
	]

	const handleNameChange = (e: any) => {
		setCurrentTodo({...currentTodo, name: e.target.value})
	}

	const handleDescriptionChange = (e: any) => {
		setCurrentTodo({...currentTodo, description: e.target.value})
	}

	const handleComplexityChange = (e: any) => {
		setCurrentTodo({...currentTodo, complexity: e.target.value})
	}

	const handleSubmit = (e: any) => {
		e.preventDefault()
		setTodoList([...todoList, currentTodo])
		setCurrentTodo({
			name: '',
			description: '',
			complexity: 0
		})
	}

	return (
		<TodoComponent>
			<FormWrapper onSubmit={handleSubmit}>
				<NameField value={currentTodo.name} onChange={handleNameChange}/>
				<DescriptionField value={currentTodo.description} onChange={handleDescriptionChange} />
				<ComplexityField value={currentTodo.complexity} onChange={handleComplexityChange}>
					{complexity.map((item: number) => {
						return <option value={item}> { item } </option>
					})}
				</ComplexityField>
				<SubmitButton> Criar </SubmitButton>
			</FormWrapper>
			<TodoCardsWrapper>
				{todoList.map((todo: ITodoCard) => {
					return (
						<TodoCard>
							<p> { todo.name } </p>
							<p> { todo.description } </p>
							<p> { todo.complexity } </p>
						</TodoCard>
					)
				})}
			</TodoCardsWrapper>
		</TodoComponent>
	)
}

export default Todo
