import styled from "styled-components"
import { device } from "../../utils/media-queries";

export const TodoComponent = styled.div`
 border: 2px solid orange;
 margin: 10px;
 ${device.s} {
 	
 }
`

export const TodoCard = styled.div`

`

export const FormWrapper = styled.form`
	display: flex;
	flex-flow: column nowrap;
	justify-content: space-between;
	padding: 20px;
	height: 200px;
`

export const NameField = styled.input`
	height: 20px;
`

export const DescriptionField = styled.input`
	height: 20px;
`

export const ComplexityField = styled.select`
	margin: 0 auto;
	width: 100px;
`

export const TodoCardsWrapper = styled.div`

`

export const SubmitButton = styled.button`

`
