export interface ITodo {

}

export interface ITodoCard {
	name: string
	description: string
	complexity: number
}
