import Layout from '../components/Layout'
import Counter from '../components/Counter'
import Chat from '../components/Chat'
import Todo from '../components/Todo'

const Home = () => {
	return(
		<Layout>
			<Counter />
			<Chat />
			<Todo />
		</Layout>
	)
}

export default Home
