const size = {
	xs: '320px',
	sm: '425px',
	m: '768px',
	l: '1024px',
	xl: '1440px',
	xxl: '2560px'
}

export const device = {
	xs: `@media (min-width: ${size.xs})`,
	s: `@media (min-width: ${size.sm})`,
	m: `@media (min-width: ${size.m})`,
	l: `@media (min-width: ${size.l})`,
	xl: `@media (min-width: ${size.xl})`,
	xxl: `@media (min-width: ${size.xxl})`
};
