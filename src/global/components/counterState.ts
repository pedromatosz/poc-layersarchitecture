export interface counterStateType {
	counter: number
}

export const counterState: counterStateType = {
	counter: 0
}
