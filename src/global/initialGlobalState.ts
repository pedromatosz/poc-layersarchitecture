import { counterState } from "./components/counterState";

export const InitialGlobalState = {
	...counterState
}

export const InitialGlobalEntityState = {

}

export type GlobalState = typeof InitialGlobalState
export type GlobalEntityState = typeof InitialGlobalEntityState
export type GlobalEntityStateKeys = keyof GlobalEntityState
